#!/bin/bash

# Check for necessary input
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <language> <sentence>"
    exit 1
fi

# Capture input
language="$1"
original_sentence="$2"

# Translate to the target language
json_payload_to_language=$(jq -n \
                              --arg language "$language" \
                              --arg initial_sentence "$original_sentence" \
                              '{ 
                                "model": "gpt-4", 
                                "messages": [
                                  { "role": "system", "content": "You are a skilled, professional translator with deep knowledge of obscure languages and high ethical standards and you reply to the point providing accurate translations whenever possible and a best guess attempt otherwise. Most importantly in your reply you return only the translation and nothing else. If the language in question has its own script you use that rather than a transliteration." },
                                  { "role": "user", "content": "Translate the following into \($language) language: \($initial_sentence)." }
                                ] 
                              }')

translated_sentence=$(curl -s 'https://api.openai.com/v1/chat/completions' \
                -H 'Content-Type: application/json' \
                -H "Authorization: Bearer $OPENAI_API_KEY" \
                -d "$json_payload_to_language" | jq -r '.choices[0].message.content')

# Translate back to English
json_payload_from_language=$(jq -n \
                              --arg sentence "$translated_sentence" \
                              '{ 
                                "model": "gpt-4", 
                                "messages": [
                                  { "role": "system", "content": "You are a skilled, professional translator with deep knowledge of obscure languages and high ethical standards and you reply to the point providing accurate translations whenever possible and a best guess attempt otherwise. Most importantly in your reply you return only the translation and nothing else. If the language in question has its own script you use that rather than a transliteration." },
                                  { "role": "user", "content": "Translate the following into English: \($sentence)" }
                                ] 
                              }')

backtranslated_sentence=$(curl -s 'https://api.openai.com/v1/chat/completions' \
                        -H 'Content-Type: application/json' \
                        -H "Authorization: Bearer $OPENAI_API_KEY" \
                        -d "$json_payload_from_language" | jq -r '.choices[0].message.content')

# Compare the sentences
json_payload_compare=$(jq -n \
                        --arg s1 "$original_sentence" \
                        --arg s2 "$backtranslated_sentence" \
                        '{ 
                          "model": "gpt-4", 
                          "messages": [
                            { "role": "system", "content": "You are a skilled, professional copy editor with deep knowledge of obscure languages and high ethical standards and you reply to the point providing accurate answers whenever possible and a best guess attempt otherwise. Most importantly in your reply you return only YES or NO and nothing else." },
                            { "role": "user", "content": "Do the following two sentences have the same meaning: 1) \($s1) 2) \($s2) ?" }
                          ] 
                        }')

verdict=$(curl -s 'https://api.openai.com/v1/chat/completions' \
                -H 'Content-Type: application/json' \
                -H "Authorization: Bearer $OPENAI_API_KEY" \
                -d "$json_payload_compare" | jq -r '.choices[0].message.content')

# Display the results
echo \" "$language" \", \" "$original_sentence" \", \" "$translated_sentence" \", \" "$backtranslated_sentence" \", \" "$verdict" \"
