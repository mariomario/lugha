# Lugha

Checking GPT4 language proficiency made easy.

## Requirements

- Bash shell (for running shell scripts)
- `curl` and `jq` (for making API requests and handling JSON responses, respectively)
- An active OpenAI API subscription (for API access)

Before running the script, please ensure you have installed `jq`, a lightweight and flexible command-line JSON processor. If you haven't installed it yet, you can do so with the following command:

```bash
sudo apt-get install jq
```

## Usage

The script requires two arguments: the target language for translation and the sentence you wish to translate. Here's how you can call the script from your terminal:

```bash
./lugha.sh <language> "<sentence>"
```

Replace `<language>` with the target language and `<sentence>` with the sentence you want to translate (enclosed in quotes).

### Example

```bash
./lugha.sh Hausa "Hello, how are you?"
```

## How It Works

1. **Translation to Target Language:** The script first translates the input sentence into the target language specified by the user. It constructs a JSON payload and sends it to the OpenAI API, receiving the translation in response.

2. **Back-Translation to English:** Next, the script translates the translated sentence back into English, following a similar process to the initial translation step, in a new session

3. **Comparison:** Finally, the script compares the original sentence and the back-translated sentence to determine if they convey the same meaning. It sends another request to the OpenAI API for this, again in a new session.

The script outputs a CSV-formatted line, displaying the target language, original sentence, translated sentence, back-translated sentence, and the verdict on whether the original and back-translated sentences are equivalent in meaning.

## Security Note

The script requires the `OPENAI_API_KEY` environment variable to be set to your OpenAI API key. Please ensure that this environment variable is securely stored and not exposed in your script or command line history.

## Datasets

I also provide two datasets built using this script showing
the results of translating ten everyday language sentences and ten
technical sentences from a variety of domains into a number of African
languages. These can be used for statistics and further analysis.

## License

Distributed under the MIT License. See `LICENSE` for more information.
